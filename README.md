# Banlinea
NOTA: tener instalado angular cli

Sobre la ruta del proyecto ejecutar: 
```
$ npm i
```
Despues de instaladas las dependencias de desarrollo ejecutar:

```
$ ng server -o
```
Se abrira un servidor de desarrollo en la ruta http://localhost:3000/banlinea

Para listar los usuarios dar click a ingresar el cual nos llevara a la ruta  de http://localhost:3000/users, donde se mostrara la interfaz de busqueda de usuarios, al ingresar no se muestran datos hasta que se comienza  a escribir en el cuadro de busqueda, una ves hecho esto comenzaran a listarse los usuarios.

En la primera lista solo se muestran datos como el nombre y la opcion de ver, la cual si se da click se desplegara la información del usuario requerida.
