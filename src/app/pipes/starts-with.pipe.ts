import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'startsWith'
})
export class StartsWithPipe implements PipeTransform {

  transform(value: any[], term: string): any[] {
    console.log(value);
    if (value.length > 0) {
      return value.filter((x: any) => x.name.toLowerCase().startsWith(term.toLowerCase()));
    } else {
      return value;
    }

  }

}
