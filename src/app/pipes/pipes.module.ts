import { NgModule } from '@angular/core';
import { StartsWithPipe } from './starts-with.pipe';
import { SubtringPipe } from './subtring.pipe';

@NgModule({
  imports: [

  ],
  declarations: [
    StartsWithPipe,
    SubtringPipe
  ],
  exports: [
    StartsWithPipe,
    SubtringPipe
  ]
})
export class PipesModule { }
