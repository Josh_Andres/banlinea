import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subtring'
})
export class SubtringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.split(' ')[0];
  }

}
