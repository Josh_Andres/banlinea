import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';


const appRoutes: Routes = [
  {path: 'banlinea', component: UsersComponent},
  {path: '**', component: NopagefoundComponent}
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes, {useHash: true});
