import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
// import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { APP_ROUTES } from './app.routes';
import { PagesModule } from './pages/pages.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceModule } from './services/service.module';
// import { OneuserComponent } from './components/oneuser/oneuser.component';
// import { PipesModule } from './pipes/pipes.module';




@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DetailComponent,
    // OneuserComponent,
  ],
  imports: [
    // PipesModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTES,
    BrowserModule,
    PagesModule,
    ServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
