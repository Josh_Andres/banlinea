import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICE } from '../config/config';


@Injectable()
export class UsersService {

  constructor(public _http: HttpClient) { }

  getUsers() {
    return this._http.get(URL_SERVICE + 'users').pipe(map((resp: any) => {
      console.log('--------<,', resp);
          return resp;
    }, (err: any) => {
      console.log('errror');
      return err;
    }));
  }

  getUser(id: any) {
    return this._http.get(URL_SERVICE + 'users/' + id ).pipe(map((resp: any) => {
            console.log(resp);
            return resp;
    }, (err: any) => {
          return err;
    }));
  }
}
