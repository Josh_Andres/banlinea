import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public users: any = [];
  public query: string;
  constructor(public _usersService: UsersService) { }

  ngOnInit() {
    this.getUSers();
  }

  getUSers() {
    this._usersService.getUsers().subscribe((resp) => {
      this.users = resp;
    }, (err) => {
      this.users = [];
      console.log('no se encuentran usuarios disponibles', err);
    });
  }

}
