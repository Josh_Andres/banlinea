import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { UserComponent } from './user/user.component';



const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [],
    children: [
      {path: 'users', component: UserComponent, data: { titulo: 'Usuarios'}},
      {path: '', redirectTo: 'banlinea', pathMatch: 'full'}
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
