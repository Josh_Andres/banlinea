import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// modulo search
import { SharedModule } from '../shared/shared.module';
// pages routes
import { PAGES_ROUTES } from './pages.routes';
import { UserComponent } from './user/user.component';
import { PagesComponent } from './pages.component';
import { CommonModule } from '@angular/common';
import { OneuserComponent } from '../components/oneuser/oneuser.component';
import { PipesModule } from '../pipes/pipes.module';





// servicios
// import { ServiceModule } from '../services/service.module';

// temporal
@NgModule({
  declarations: [
  OneuserComponent,
  PagesComponent,
  UserComponent
  ],
  exports: [
    UserComponent
  ],
  imports: [
    PipesModule,
    CommonModule,
    SharedModule,
    PAGES_ROUTES,
    FormsModule
  ]
})

export class PagesModule {}
