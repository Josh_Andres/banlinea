import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-oneuser',
  templateUrl: './oneuser.component.html',
  styleUrls: ['./oneuser.component.scss']
})
export class OneuserComponent implements OnInit {
  @Input() user: any;
  public detailUser: any = [];
  public click: boolean;
  constructor(public router: Router, public _usersServices: UsersService) { }

  ngOnInit() {
  }
  detail(id: any) {
      this.click = true;
     this._usersServices.getUser(id).subscribe((resp) => {
        console.log('ok');
        this.detailUser = resp;
     }, (err) => {
        this.detailUser = [];
        console.log('error', err);
     });
  }
}
